![](https://img.shields.io/badge/Code-Python-informational?style=flat&logo=python&logoColor=white&color=2bbc8a)

<h1>Week 2 (14 Oct)</h1>

<details>
    <summary>Easy Challenge</summary>

If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. Find the sum of all the multiples of 3 or 5 below 1000.
</details><br>

<details>
    <summary>Medium Challenge</summary>

Write a function that prints a string in a frame, such as: frame('Hello World in a frame') prints:

    *********
    * Hello *
    * World *
    * in    *
    * a     *
    * frame *
    *********
</details><br>

<details>
    <summary>Hard Challenge</summary>
    
Determine if a word or phrase is an isogram.

An isogram (also known as a "nonpattern word") is a word or phrase without a repeating letter, however spaces and hyphens are allowed to appear multiple times.

Examples of isograms:

    lumberjacks
    background
    downstream
    six-year-old

The word isograms, however, is not an isogram, because the s repeats.
</details><br>

The python files above may be used a starting point, or you may build your own solutions from scratch.