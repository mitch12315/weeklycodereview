![](https://img.shields.io/badge/OS-Linux-informational?style=flat&logo=linux&logoColor=white&color=2bbc8a)

![](https://img.shields.io/badge/Platform-Ansible-informational?style=flat&logo=ansible&logoColor=white&color=2bbc8a)

![](https://img.shields.io/badge/Code-Python-informational?style=plastic&logo=python&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Code-Jinja2-informational?style=flat&logo=jinja&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Code-JSON-informational?style=flat&logo=json&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Code-YAML-informational?style=flat&logo=&logoColor=white&color=2bbc8a)

<h1>WeeklyCodeReview</h1>

Repository for storing and sharing weekly coding challenges and solutions.

<h2>Directions</h2>

- In order to share with the community of users on this site, create a branch for your solution to the weekly code challenge.

- Once you have a solution, save it back to this repo for review during the weekly meetup.

- There is no need to submit a merge request for your branch.


<h2>Resources</h2>

- Getting started with Visual Studio code (VScode) editor: https://www.youtube.com/watch?v=VqCgcpAypFQ
- Getting started with Python: https://www.tutorialspoint.com/python3/python3_whatisnew.htm
- Getting started with Gitlab: https://docs.gitlab.com/ee/gitlab-basics
