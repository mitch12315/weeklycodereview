![](https://img.shields.io/badge/OS-Linux-informational?style=flat&logo=linux&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Platform-Ansible-informational?style=flat&logo=ansible&logoColor=white&color=2bbc8a)
![](https://img.shields.io/badge/Code-Jinja2-informational?style=flat&logo=jinja&logoColor=white&color=2bbc8a)

<h1>Week 3 (21 Oct)</h1>

<details>
    <summary>Easy Challenge</summary>
Successfully install ansible on a linux host or VM.  Clone this repo to the linux host or VM.
</details><br>

<details>
    <summary> Medium Challenge</summary>
Edit the existing Jinja template so that it prints the first name, last name, and grade for each student.  The first name is already in the template file as an example.
</details><br>

<details>
    <summary>Hard Challenge</summary>
Create a detailed course schedule report that lists the classes for each student.  Also include the date and time that the report was created.  The basic outline is already in the hard template file.
</details><br>

<details>
    <summary>Resources</summary>
 - <a href="https://docs.ansible.com/ansible/latest/index.html">Ansible documentation</a><br>
 - <a href="https://www.youtube.com/watch?v=GG8ToAngabQ">Ansible intro video</a><br>
 - <a href="https://jinja.palletsprojects.com/en/3.0.x/">Jinja documentation</a>
</details><br>

The project includes an "ansible_files" directory that holds all of the ansible files to run the playbook.  The playbook runs as is and should only require changes to your templates in the templates directory.  Your finished files built off the templates will be stored in the "playbook_output" folder.  "main.yml" is the actual playbook and can be run from the terminal line with the command: ansible-playbook main.yml
